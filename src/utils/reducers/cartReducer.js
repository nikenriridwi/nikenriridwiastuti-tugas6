const initialState = {
  cart: [],
};

const cartReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'ADD_CART':
      return {
        ...state,
        cart: action.data,
      };
    case 'UPDATE_CART':
      var newData = [...state.cart];
      var findIndex = state.cart.findIndex(value => {
        return value.id === action.data.id;
      });

      newData[findIndex] = action.data;
      return {
        ...state,
        cart: newData,
      };
    case 'DELETE_CART':
      var newData = [...state.cart];
      var findIndex = state.cart.findIndex(value => {
        return value.id === action.id;
      });

      newData.splice(findIndex, 1);
      return {
        ...state,
        cart: newData,
      };
    default:
      return state;
  }
};

export default cartReducer;
