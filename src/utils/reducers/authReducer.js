const initialState = {
  userData: {
    nama: 'Niken Riri',
    email: 'nikenriri05@gmail.com',
    phoneNumber: '085725127872',
    address:
      'Jl. Tambakbayan TB VII No.14B, Rt.7/Rw.3, Caturtunggal, Depok, Sleman, DI Yogyakarta',
  },
  //isLoggedIn: true,
};

const authReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'LOGIN_SUCCESS':
      return {
        ...state,
        isLoggedIn: true,
        userData: {...action.data},
      };
    case 'LOGOUT':
      return {
        ...state,
        userData: {
          nama: '',
          email: '',
          phoneNumber: '',
          address: '',
          password: '',
        },
        isLoggedIn: false,
      };
    default:
      return state;
  }
};
export default authReducer;
