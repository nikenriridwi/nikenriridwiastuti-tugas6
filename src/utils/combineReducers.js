import {combineReducers} from 'redux';
import dataReducer from './reducers/dataReducer';
import cartReducer from './reducers/cartReducer';
import authReducer from './reducers/authReducer';

const rootReducer = combineReducers({
  data: dataReducer,
  cart: cartReducer,
  auth: authReducer,
});

export default rootReducer;
