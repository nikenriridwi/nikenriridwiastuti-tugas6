import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import TransactionScreen from './screen/Transaction';
import DetailTransactionScreen from './screen/DetailTransaction';
import CheckoutScreen from './screen/Checkout';

const Stack = createStackNavigator();

function TransactionNavigation() {
  return (
    <Stack.Navigator screenOptions={{headerShown: false}}>
      <Stack.Screen
        name="Transaksi"
        component={TransactionScreen}
        options={{headerShown: true}}
      />
      <Stack.Screen
        name=" "
        component={DetailTransactionScreen}
        options={{headerShown: true}}
      />
      <Stack.Screen
        name="Checkout"
        component={CheckoutScreen}
        options={{headerShown: true}}
      />
    </Stack.Navigator>
  );
}

export default TransactionNavigation;
