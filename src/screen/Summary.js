import React from 'react';
import {
  View,
  Text,
  Image,
  ScrollView,
  TouchableOpacity,
  FlatList,
} from 'react-native';
import {useSelector} from 'react-redux';

const SummaryScreen = ({navigation, route}) => {
  const {userData} = useSelector(state => state.auth);
  const {store} = useSelector(state => state.data);
  const {cart} = useSelector(state => state.cart);

  return (
    <View style={{flex: 1, backgroundColor: '#F6F8FF'}}>
      <ScrollView>
        <View
          style={{
            width: '100%',
            height: 153,
            backgroundColor: 'white',
            marginTop: 2,
            //marginLeft: 26,
          }}>
          <Text
            style={{
              color: '#979797',
              fontSize: 14,
              fontWeight: '500',
              marginTop: 18,
              marginLeft: 26,
            }}>
            Data Customer
          </Text>
          <Text
            style={{
              color: '#201F26',
              fontSize: 14,
              fontWeight: '400',
              marginTop: 10,
              marginLeft: 26,
            }}>
            {userData.nama} ({userData.phoneNumber})
          </Text>
          <Text
            style={{
              color: '#201F26',
              fontSize: 14,
              fontWeight: '400',
              marginTop: 10,
              marginLeft: 26,
            }}>
            {userData.address}
          </Text>
          <Text
            style={{
              color: '#201F26',
              fontSize: 14,
              fontWeight: '400',
              marginTop: 10,
              marginLeft: 26,
            }}>
            {userData.email}
          </Text>
        </View>

        <FlatList
          data={store}
          keyExtractor={(item, index) => index.toString()}
          renderItem={({item, index}) => (
            <View
              style={{
                width: '100%',
                height: 107,
                backgroundColor: 'white',
                marginTop: 12,
                //marginLeft: 26,
              }}>
              <Text
                style={{
                  color: '#979797',
                  fontSize: 14,
                  fontWeight: '500',
                  marginTop: 18,
                  marginLeft: 26,
                }}>
                Alamat Outlet Tujuan
              </Text>
              <Text
                style={{
                  color: '#201F26',
                  fontSize: 14,
                  fontWeight: '400',
                  marginTop: 10,
                  marginLeft: 26,
                }}>
                {item.storeName} (027-343457)
              </Text>
              <Text
                style={{
                  color: '#201F26',
                  fontSize: 14,
                  fontWeight: '400',
                  marginTop: 10,
                  marginLeft: 26,
                }}>
                {item.address}
              </Text>
            </View>
          )}
        />

        <FlatList
          data={cart}
          keyExtractor={(item, index) => index.toString()}
          renderItem={({item, index}) => (
            <View
              style={{
                width: '100%',
                height: 172,
                backgroundColor: '#fff',
                marginTop: 12,
                justifyContent: 'center',
              }}>
              <Text
                style={{
                  color: '#979797',
                  fontSize: 14,
                  fontWeight: '500',
                  marginLeft: 26,
                }}>
                Barang
              </Text>
              <View style={{flexDirection: 'row'}}>
                <View style={{marginTop: 16, marginLeft: 24}}>
                  <Image
                    source={{uri: item.poto}}
                    style={{width: 84, height: 84, resizeMode: 'contain'}}
                  />
                </View>
                <View
                  style={{
                    marginLeft: 15,
                    width: '60%',
                    justifyContent: 'center',
                  }}>
                  <Text
                    style={{color: '#000', fontSize: 12, fontWeight: '500'}}>
                    {item.merek} - {item.warna} - {item.ukuran}
                  </Text>
                  <Text
                    style={{
                      marginTop: 11,
                      color: '#737373',
                      fontSize: 12,
                      fontWeight: '400',
                    }}>
                    {item.layanan}
                  </Text>
                  <Text
                    style={{
                      color: '#737373',
                      fontSize: 12,
                      fontWeight: '400',
                      marginTop: 11,
                    }}>
                    Note : {item.catatan}
                  </Text>
                </View>
              </View>
            </View>
          )}
        />
      </ScrollView>
      <TouchableOpacity
        style={{
          //width: '100%',
          marginTop: 35,
          backgroundColor: '#BB2427',
          borderRadius: 8,
          paddingVertical: 15,
          justifyContent: 'center',
          alignItems: 'center',
          marginBottom: 34,
          marginHorizontal: 20,
        }}
        onPress={() =>
          navigation.navigate('HomeNavigation', {
            screen: 'Berhasil',
          })
        }>
        <Text
          style={{
            color: '#fff',
            fontSize: 16,
            fontWeight: 'bold',
          }}>
          Reservasi Sekarang
        </Text>
      </TouchableOpacity>
    </View>
  );
};

export default SummaryScreen;
