import React, {useState, Component} from 'react';
import {
  View,
  Text,
  TextInput,
  Image,
  TouchableOpacity,
  ScrollView,
  StyleSheet,
  Alert,
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import AntDesign from 'react-native-vector-icons/AntDesign';
import {useDispatch, useSelector} from 'react-redux';
import moment from 'moment/moment';

const addDataScreen = ({navigation, route}) => {
  // console.log('item: ', route.params.item);
  const [availableOpen, setAvailableOpen] = useState('');
  const [availableClose, setAvailableClose] = useState('');
  const [description, setDescription] = useState('');
  const [address, setAddress] = useState('');
  const [minimum, setMinimum] = useState('');
  const [maximum, setMaximum] = useState('');
  const [rating, setRatings] = useState('');
  const [image, setImage] = useState('');
  const [name, setName] = useState('');
  const dispatch = useDispatch();
  const {store} = useSelector(state => state.data);

  const [selectedOption, setSelectedOption] = useState(null);

  const handleOptionSelect = option => {
    setSelectedOption(option);
  };

  const storeData = () => {
    // var date = new Date();
    var dataStore = [...store];
    const data = {
      id: moment(new Date()).format('YYYYMMDDHHmmssSSS'),
      address: address,
      open: availableOpen,
      close: availableClose,
      description: description,
      minimumPrice: minimum,
      maximumPrice: maximum,
      ratings: rating,
      storeImage: image,
      storeName: name,
      isOpen: selectedOption === 'buka',
    };
    dataStore.push(data);
    dispatch({type: 'ADD_DATA', data: dataStore});
    navigation.goBack();
  };

  const checkData = () => {
    if (route.params) {
      const data = route.params.item;
      setAddress(data.address);
      setAvailableOpen(data.open);
      setAvailableClose(data.close);
      setDescription(data.description);
      setMinimum(data.minimumPrice);
      setMaximum(data.maximumPrice);
      setRatings(data.ratings);
      setImage(data.storeImage);
      setName(data.storeName);
    }
  };

  const updateData = () => {
    const data = {
      id: route.params.item.id,
      address: address,
      open: availableOpen,
      close: availableClose,
      description: description,
      minimumPrice: minimum,
      maximumPrice: maximum,
      ratings: rating,
      storeImage: image,
      storeName: name,
      isOpen: selectedOption === 'buka',
    };
    dispatch({type: 'UPDATE_DATA', data});
    navigation.goBack();
  };

  const deleteData = () => {
    dispatch({type: 'DELETE_DATA', id: route.params.item.id});
    navigation.goBack();
  };

  React.useEffect(() => {
    checkData();
  }, []);

  return (
    <View style={{flex: 1, backgroundColor: '#fff'}}>
      <View style={styles.header}>
        <TouchableOpacity
          style={{
            width: '15%',
            justifyContent: 'center',
            alignItems: 'center',
          }}
          onPress={() => navigation.goBack()}>
          <Icon name="arrow-left" size={25} color="black" />
        </TouchableOpacity>
        <View
          style={{
            width: '70%',
            justifyContent: 'center',
            //alignItems: 'center',
          }}>
          <Text style={{fontSize: 16, fontWeight: '600', color: 'black'}}>
            {route.params ? 'Ubah Data' : 'Tambah Data'}
          </Text>
        </View>
        <View style={{width: '15%'}} />
      </View>
      <ScrollView>
        <View>
          <Text
            style={{
              color: '#BB2427',
              fontSize: 12,
              fontWeight: '600',
              marginTop: 25,
              marginLeft: 21,
            }}>
            Nama Toko
          </Text>
          <TextInput
            style={{
              backgroundColor: '#F6F8FF',
              borderRadius: 7,
              marginHorizontal: 20,
              marginTop: 11,
            }}
            value={name}
            placeholder="Masukkan Nama Toko"
            onChangeText={text => setName(text)}
          />
        </View>
        <View>
          <Text
            style={{
              color: '#BB2427',
              fontSize: 12,
              fontWeight: '600',
              marginTop: 25,
              marginLeft: 21,
            }}>
            Alamat
          </Text>
          <TextInput
            style={{
              backgroundColor: '#F6F8FF',
              borderRadius: 7,
              marginHorizontal: 20,
              marginTop: 11,
            }}
            value={address}
            placeholder="Masukkan Alamat"
            onChangeText={text => setAddress(text)}
          />
        </View>
        <View>
          <Text
            style={{
              color: '#BB2427',
              fontSize: 12,
              fontWeight: '600',
              marginTop: 25,
              marginLeft: 21,
            }}>
            Status Buka Tutup
          </Text>
          <View style={{flexDirection: 'row', marginLeft: 20, marginTop: 10}}>
            <TouchableOpacity onPress={() => handleOptionSelect('buka')}>
              <View style={{flexDirection: 'row', alignItems: 'center'}}>
                <Icon
                  name={selectedOption === 'buka' ? 'check-circle' : 'circle-o'}
                  size={24}
                  color="#BB2427"
                />
                <Text
                  style={{
                    marginLeft: 8,
                    color: selectedOption === 'buka' ? '#BB2427' : 'black',
                  }}>
                  Buka
                </Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => handleOptionSelect('tutup')}>
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  marginLeft: 110,
                }}>
                <Icon
                  name={
                    selectedOption === 'tutup' ? 'check-circle' : 'circle-o'
                  }
                  size={24}
                  color="#BB2427"
                />
                <Text
                  style={{
                    marginLeft: 8,
                    color: selectedOption === 'tutup' ? '#BB2427' : 'black',
                  }}>
                  Tutup
                </Text>
              </View>
            </TouchableOpacity>
          </View>
        </View>
        <View style={{flexDirection: 'row'}}>
          <View>
            <Text
              style={{
                color: '#BB2427',
                fontSize: 12,
                fontWeight: '600',
                marginTop: 25,
                marginLeft: 21,
              }}>
              Jam Buka
            </Text>
            <TextInput
              style={{
                backgroundColor: '#F6F8FF',
                borderRadius: 7,
                marginLeft: 20,
                width: 150,
                marginTop: 11,
              }}
              value={availableOpen}
              placeholder="Pilih Jam"
              onChangeText={text => setAvailableOpen(text)}
            />
          </View>
          <View>
            <Text
              style={{
                color: '#BB2427',
                fontSize: 12,
                fontWeight: '600',
                marginTop: 25,
                marginLeft: 20,
              }}>
              Jam Tutup
            </Text>
            <TextInput
              style={{
                backgroundColor: '#F6F8FF',
                borderRadius: 7,
                width: 150,
                marginLeft: 20,
                marginTop: 11,
              }}
              value={availableClose}
              placeholder="Pilih Jam"
              onChangeText={text => setAvailableClose(text)}
            />
          </View>
        </View>

        <View>
          <Text
            style={{
              color: '#BB2427',
              fontSize: 12,
              fontWeight: '600',
              marginTop: 25,
              marginLeft: 21,
            }}>
            Jumlah Rating
          </Text>
          <TextInput
            style={{
              backgroundColor: '#F6F8FF',
              borderRadius: 7,
              marginHorizontal: 20,
              marginTop: 11,
            }}
            value={rating}
            placeholder="Masukkan Jumlah Rating"
            onChangeText={text => setRatings(text)}
          />
        </View>

        <View style={{flexDirection: 'row'}}>
          <View>
            <Text
              style={{
                color: '#BB2427',
                fontSize: 12,
                fontWeight: '600',
                marginTop: 25,
                marginLeft: 21,
              }}>
              Harga Minimal
            </Text>
            <TextInput
              style={{
                backgroundColor: '#F6F8FF',
                borderRadius: 7,
                marginLeft: 20,
                width: 150,
                marginTop: 11,
              }}
              value={minimum}
              onChangeText={text => setMinimum(text)}
            />
          </View>
          <View>
            <Text
              style={{
                color: '#BB2427',
                fontSize: 12,
                fontWeight: '600',
                marginTop: 25,
                marginLeft: 20,
              }}>
              Harga Maksimal
            </Text>
            <TextInput
              style={{
                backgroundColor: '#F6F8FF',
                borderRadius: 7,
                width: 150,
                marginLeft: 20,
                marginTop: 11,
              }}
              value={maximum}
              onChangeText={text => setMaximum(text)}
            />
          </View>
        </View>

        <View>
          <Text
            style={{
              color: '#BB2427',
              fontSize: 12,
              fontWeight: '600',
              marginTop: 25,
              marginLeft: 21,
            }}>
            Deskripsi
          </Text>
          <TextInput
            style={{
              backgroundColor: '#F6F8FF',
              borderRadius: 7,
              marginHorizontal: 20,
              marginTop: 11,
              height: 92,
              textAlignVertical: 'top',
            }}
            value={description}
            placeholder="Masukkan Deskripsi"
            onChangeText={text => setDescription(text)}
          />
        </View>

        <View>
          <Text
            style={{
              color: '#BB2427',
              fontSize: 12,
              fontWeight: '600',
              marginTop: 25,
              marginLeft: 21,
            }}>
            Gambar Toko
          </Text>

          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              backgroundColor: '#F6F8FF',
              marginTop: 20,
              marginRight: 20,
              borderRadius: 10,
              marginLeft: 20,
            }}>
            <AntDesign
              name="jpgfile1"
              size={20}
              style={{
                width: 20,
                height: 20,
                marginLeft: 10,
                resizeMode: 'contain',
              }}
            />
            <TextInput
              style={{
                backgroundColor: '#F6F8FF',
                borderRadius: 7,
                //marginHorizontal: 20,
                //marginTop: 11,
              }}
              value={image}
              placeholder="Pilih Gambar"
              onChangeText={text => setImage(text)}
            />
          </View>
        </View>
      </ScrollView>

      <TouchableOpacity
        style={{
          //width: '100%',
          marginTop: 35,
          backgroundColor: '#BB2427',
          borderRadius: 8,
          paddingVertical: 15,
          justifyContent: 'center',
          alignItems: 'center',
          marginBottom: 34,
          marginHorizontal: 20,
        }}
        onPress={() => {
          if (route.params) {
            updateData();
          } else {
            storeData();
          }
        }}>
        <Text
          style={{
            color: '#fff',
            fontSize: 16,
            fontWeight: 'bold',
          }}>
          {route.params ? 'Ubah' : 'Simpan'}
        </Text>
      </TouchableOpacity>

      {route.params && (
        <TouchableOpacity
          style={{
            //width: '100%',
            //marginTop: 35,
            backgroundColor: '#BB2427',
            borderRadius: 8,
            paddingVertical: 15,
            justifyContent: 'center',
            alignItems: 'center',
            marginBottom: 34,
            marginHorizontal: 20,
          }}
          onPress={deleteData}>
          <Text
            style={{
              color: '#fff',
              fontSize: 16,
              fontWeight: 'bold',
            }}>
            Hapus
          </Text>
        </TouchableOpacity>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  header: {
    width: '100%',
    flexDirection: 'row',
    //alignItems: 'center',
    backgroundColor: 'white',
    paddingVertical: 15,
  },
});

export default addDataScreen;
