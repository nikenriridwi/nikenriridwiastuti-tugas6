import React from 'react';
import {
  View,
  Text,
  Image,
  ScrollView,
  TouchableOpacity,
  FlatList,
} from 'react-native';
import {useSelector} from 'react-redux';

const ChartScreen = ({navigation, route}) => {
  const {cart} = useSelector(state => state.cart);
  console.log('item: ', cart);
  return (
    <View style={{flex: 1, backgroundColor: '#F6F8FF'}}>
      <FlatList
        data={cart}
        keyExtractor={(item, index) => index.toString()}
        renderItem={({item, index}) => (
          <View
            style={{
              // height: 133,
              backgroundColor: '#fff',
              borderRadius: 8,
              margin: 11,
              justifyContent: 'center',
            }}>
            <View style={{flexDirection: 'row'}}>
              <View style={{marginVertical: 6, marginLeft: 6}}>
                <Image
                  source={{uri: item.poto}}
                  style={{width: 84, height: 84, resizeMode: 'contain'}}
                />
              </View>
              <View
                style={{
                  marginLeft: 15,
                  width: '60%',
                  justifyContent: 'center',
                }}>
                <Text style={{color: '#000', fontSize: 12, fontWeight: '500'}}>
                  {item.merek} - {item.warna} - {item.ukuran}
                </Text>
                <Text
                  style={{
                    marginTop: 4,
                    color: '#737373',
                    fontSize: 12,
                    fontWeight: '400',
                  }}>
                  {item.layanan}
                </Text>
                <Text
                  style={{color: '#737373', fontSize: 12, fontWeight: '400'}}>
                  Note : {item.catatan}
                </Text>
              </View>
            </View>
          </View>
        )}
      />

      <TouchableOpacity
        style={{
          flexDirection: 'row',
          justifyContent: 'center',
          marginTop: 45,
        }}>
        <Image
          source={require('../assets/icons/Plus.png')}
          style={{width: 20, height: 20}}
        />
        <Text
          style={{
            color: '#BB2427',
            fontSize: 14,
            fontWeight: '700',
            marginLeft: 8,
          }}>
          Tambah Barang
        </Text>
      </TouchableOpacity>
      <View>
        <TouchableOpacity
          style={{
            //width: '100%',
            marginTop: 35,
            backgroundColor: '#BB2427',
            borderRadius: 8,
            paddingVertical: 15,
            justifyContent: 'center',
            alignItems: 'center',
            marginBottom: 34,
            marginHorizontal: 20,
          }}
          onPress={() =>
            navigation.navigate('HomeNavigation', {
              screen: 'Summary',
            })
          }>
          <Text
            style={{
              color: '#fff',
              fontSize: 16,
              fontWeight: 'bold',
            }}>
            Selanjutnya
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default ChartScreen;
