import React from 'react';
import {
  View,
  Text,
  ScrollView,
  KeyboardAvoidingView,
  Image,
  TextInput,
  TouchableOpacity,
  StyleSheet,
  Dimensions,
  ImageBackground,
  FlatList,
} from 'react-native';
import {useSelector} from 'react-redux';

const DetailScreen = ({navigation, route}) => {
  const {item} = route.params;
  return (
    <View style={{flex: 1, backgroundColor: '#fff'}}>
      <ScrollView //component yang digunakan agar tampilan kita bisa discroll
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{paddingBottom: 10}}>
        <KeyboardAvoidingView //component yang digunakan untuk mengatur agar keyboard tidak menutupi
          behavior="padding" //tampilan form atau text input
          enabled
          keyboardVerticalOffset={-500}>
          <ImageBackground
            source={{uri: item.storeImage}} //load atau panggil asset image dari local
            style={{
              width: Dimensions.get('window').width, //atur agar lebar gambar adalah selebar layar device
              height: 317,
            }}>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center',
                marginTop: 28,
                marginHorizontal: 24,
              }}>
              <TouchableOpacity onPress={() => navigation.navigate('Home')}>
                <Image
                  source={require('../assets/icons/back.png')}
                  style={{width: 24, height: 24}}
                />
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() =>
                  navigation.navigate('HomeNavigation', {
                    screen: 'Keranjang',
                  })
                }>
                <Image
                  source={require('../assets/icons/Bag.png')}
                  style={{tintColor: 'white', width: 24, height: 24}}
                />
              </TouchableOpacity>
            </View>
          </ImageBackground>

          <View
            style={{
              width: '100%',
              backgroundColor: '#fff',
              borderTopLeftRadius: 19,
              borderTopRightRadius: 19,
              //paddingHorizontal: 20,
              paddingTop: 24,
              marginTop: -20,
            }}>
            <View style={{width: 225, height: 34, marginLeft: 33}}>
              <Text
                style={{
                  color: '#201F26',
                  fontSize: 18,
                  fontWeight: '700',
                  lineHeight: 17,
                  letterSpacing: 0.257,
                }}>
                {item.storeName}
              </Text>
              <Image
                source={require('../assets/icons/star.png')}
                style={{
                  resizeMode: 'contain',
                  width: 65,
                  height: 10,
                  marginTop: 2,
                  marginLeft: -4,
                }}
              />
            </View>
            <View
              style={{
                flexDirection: 'row',
                marginHorizontal: 33,
                marginTop: 10,
              }}>
              <Image
                source={require('../assets/icons/location.png')}
                style={{width: 25, height: 25, marginLeft: -4}}
              />
              <Text
                style={{
                  color: '#979797',
                  fontSize: 10,
                  fontWeight: '400',
                  lineHeight: 17,
                  letterSpacing: 0.257,
                }}>
                {item.address}
              </Text>
              <Text
                style={{
                  color: '#3471CD',
                  fontSize: 12,
                  fontWeight: '700',
                  lineHeight: 17,
                  letterSpacing: 0.257,
                  marginLeft: 16,
                }}>
                Lihat Maps
              </Text>
            </View>

            <View
              style={{
                flexDirection: 'row',
                marginTop: 15,
                marginLeft: 33,
              }}>
              <Text
                style={
                  item.isOpen
                    ? {
                        backgroundColor: 'rgba(17, 168, 78, 0.12)',
                        borderRadius: 10,
                        width: 58,
                        padding: 3,
                        //marginTop: 20,
                        textAlign: 'center',
                        color: '#11A84E',
                        fontSize: 12,
                        fontWeight: '700',
                      }
                    : {
                        backgroundColor: 'rgba(230, 76, 60, 0.20)',
                        borderRadius: 10,
                        width: 58,
                        padding: 3,
                        //marginTop: 20,
                        textAlign: 'center',
                        color: '#EA3D3D',
                        fontSize: 12,
                        fontWeight: '700',
                      }
                }>
                {item.isOpen ? 'BUKA' : 'TUTUP'}
              </Text>
              <Text
                style={{
                  color: '#343434',
                  fontSize: 12,
                  fontWeight: '700',
                  lineHeight: 17,
                  letterSpacing: 0.257,
                  marginLeft: 15,
                  textAlign: 'center',
                }}>
                {item.open} - {item.close}
              </Text>
            </View>

            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                marginTop: 17,
              }}>
              <View style={{flex: 1, height: 1, backgroundColor: '#EEE'}} />
              <View style={{flex: 1, height: 1, backgroundColor: '#EEE'}} />
            </View>

            <View>
              <Text
                style={{
                  color: '#201F26',
                  fontSize: 16,
                  fontWeight: '500',
                  lineHeight: 17,
                  letterSpacing: 0.257,
                  marginTop: 23,
                  marginHorizontal: 33,
                }}>
                Deskripsi
              </Text>
              <Text
                style={{
                  color: '#595959',
                  fontSize: 14,
                  fontWeight: '400',
                  lineHeight: 17,
                  letterSpacing: 0.257,
                  marginTop: 10,
                  marginHorizontal: 33,
                }}>
                {item.description}
              </Text>
              <Text
                style={{
                  color: '#201F26',
                  fontSize: 16,
                  fontWeight: '500',
                  lineHeight: 17,
                  letterSpacing: 0.257,
                  marginTop: 23,
                  marginHorizontal: 33,
                }}>
                Range Biaya
              </Text>
              <Text
                style={{
                  color: '#8D8D8D',
                  fontSize: 16,
                  fontWeight: '500',
                  lineHeight: 17,
                  letterSpacing: 0.257,
                  marginTop: 6,
                  marginHorizontal: 33,
                }}>
                Rp {item.minimumPrice} - {item.maximumPrice}
              </Text>
            </View>
          </View>
        </KeyboardAvoidingView>
      </ScrollView>
      <TouchableOpacity
        style={{
          //width: '100%',
          marginTop: 35,
          backgroundColor: '#BB2427',
          borderRadius: 8,
          paddingVertical: 15,
          justifyContent: 'center',
          alignItems: 'center',
          marginBottom: 34,
          marginHorizontal: 20,
        }}
        onPress={() =>
          navigation.navigate('HomeNavigation', {
            screen: 'Formulir Pendaftaran',
          })
        }>
        <Text
          style={{
            color: '#fff',
            fontSize: 16,
            fontWeight: 'bold',
          }}>
          Repair Disini
        </Text>
      </TouchableOpacity>
    </View>
  );
};

export default DetailScreen;
