import React, {useState} from 'react';
import {
  View,
  Text,
  TextInput,
  Image,
  TouchableOpacity,
  ScrollView,
  StyleSheet,
  FlatList,
} from 'react-native';
import CheckBox from '@react-native-community/checkbox';
import {useDispatch, useSelector} from 'react-redux';
import moment from 'moment/moment';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Icon from 'react-native-vector-icons/FontAwesome';

const FormulirScreen = ({navigation, route}) => {
  const [checked, setChecked] = useState(null);

  const [toggleCheckBox, setToggleCheckBox] = useState([
    {key: 'Ganti Sol Sepatu', checked: false},
    {key: 'Jahit Sepatu', checked: false},
    {key: 'Repaint Sepatu', checked: false},
    {key: 'Cuci Sepatu', checked: false},
  ]);

  const handleItemPress = index => {
    const UpdateItem = toggleCheckBox.map((item, i) => ({
      ...item,
      checked: i === index,
    }));
    setToggleCheckBox(UpdateItem);
  };

  const getService = () => {
    const service = toggleCheckBox.filter(item => item.checked);
    return service[0].key;
  };

  //console.log('item: ', route.params.item);
  const [merek, setMerek] = useState('');
  const [warna, setWarna] = useState('');
  const [ukuran, setUkuran] = useState('');
  const [poto, setPoto] = useState('');
  const [layanan, setLayanan] = useState('');
  const [catatan, setCatatan] = useState('');
  const dispatch = useDispatch();
  const {cart} = useSelector(state => state.cart);

  const cartData = () => {
    // var date = new Date();
    var dataCart = [...cart];
    const data = {
      id: moment(new Date()).format('YYYYMMDDHHmmssSSS'),
      merek: merek,
      warna: warna,
      ukuran: ukuran,
      poto: poto,
      layanan: getService(),
      catatan: catatan,
    };
    dataCart.push(data);
    dispatch({type: 'ADD_CART', data: dataCart});
    navigation.navigate('HomeNavigation', {screen: 'Keranjang'});
  };

  const checkCart = () => {
    if (route.params) {
      const data = route.params.item;
      setMerek(data.merek);
      setWarna(data.warna);
      setUkuran(data.ukuran);
      setLayanan(data.layanan);
      setCatatan(data.catatan);
    }
  };

  const updateCart = () => {
    const data = {
      id: route.params.item.id,
      merek: merek,
      warna: warna,
      ukuran: ukuran,
      poto: poto,
      layanan: layanan,
      catatan: catatan,
    };
    dispatch({type: 'UPDATE_CART', data});
    navigation.goBack();
  };

  const deleteCart = () => {
    dispatch({type: 'DELETE_CART', id: route.params.item.id});
    navigation.goBack();
  };

  React.useEffect(() => {
    checkCart();
  }, []);

  return (
    <View style={{flex: 1, backgroundColor: '#fff'}}>
      <ScrollView>
        <View>
          <Text
            style={{
              color: '#BB2427',
              fontSize: 12,
              fontWeight: '600',
              marginTop: 25,
              marginLeft: 21,
            }}>
            Merek
          </Text>
          <TextInput
            style={{
              backgroundColor: '#F6F8FF',
              borderRadius: 7,
              marginHorizontal: 20,
              marginTop: 11,
            }}
            value={merek}
            onChangeText={text => setMerek(text)}
            placeholder="Masukkan Merk Barang"
          />
        </View>
        <View>
          <Text
            style={{
              color: '#BB2427',
              fontSize: 12,
              fontWeight: '600',
              marginTop: 25,
              marginLeft: 21,
            }}>
            Warna
          </Text>
          <TextInput
            style={{
              backgroundColor: '#F6F8FF',
              borderRadius: 7,
              marginHorizontal: 20,
              marginTop: 11,
            }}
            value={warna}
            onChangeText={text => setWarna(text)}
            placeholder="Warna Barang, cth: Merah - Putih"
          />
        </View>
        <View>
          <Text
            style={{
              color: '#BB2427',
              fontSize: 12,
              fontWeight: '600',
              marginTop: 25,
              marginLeft: 21,
            }}>
            Ukuran
          </Text>
          <TextInput
            style={{
              backgroundColor: '#F6F8FF',
              borderRadius: 7,
              marginHorizontal: 20,
              marginTop: 11,
            }}
            value={ukuran}
            onChangeText={text => setUkuran(text)}
            placeholder="Cth: S, M/39,40"
          />
        </View>

        <Text
          style={{
            color: '#BB2427',
            fontSize: 12,
            fontWeight: '600',
            marginTop: 25,
            marginLeft: 21,
          }}>
          Poto
        </Text>
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            backgroundColor: '#F6F8FF',
            marginTop: 20,
            marginRight: 20,
            borderRadius: 10,
            marginLeft: 20,
          }}>
          <AntDesign
            name="jpgfile1"
            size={20}
            style={{
              width: 20,
              height: 20,
              marginLeft: 10,
              resizeMode: 'contain',
            }}
          />
          <TextInput
            style={{
              backgroundColor: '#F6F8FF',
              borderRadius: 7,
              //marginHorizontal: 20,
              //marginTop: 11,
            }}
            value={poto}
            placeholder="Pilih Gambar"
            onChangeText={text => setPoto(text)}
          />
        </View>

        <View style={{marginLeft: 20, marginTop: 55}}>
          <FlatList
            data={toggleCheckBox}
            renderItem={({item, index}) => (
              <View style={{flexDirection: 'row', alignItems: 'center'}}>
                <TouchableOpacity
                  activeOpacity={1}
                  style={{
                    width: 24,
                    height: 24,
                    borderRadius: 3,
                    borderColor: '#B8B8B8',
                    marginTop: 10,
                  }}
                  onPress={() => {
                    handleItemPress(index);
                  }}>
                  {item.checked ? (
                    <AntDesign
                      name="checksquareo"
                      size={25}
                      style={{color: '#BB2427'}}
                    />
                  ) : (
                    <Icon name="square" size={24} style={{color: '#CCC8AA'}} />
                  )}
                </TouchableOpacity>
                <Text
                  style={{
                    fontSize: 14,
                    color: '#201f26',
                    fontWeight: '400',
                    lineHeight: 17,
                    letterSpacing: 0.257,
                    marginTop: 10,
                  }}>
                  {item.key}
                </Text>
              </View>
            )}
          />
        </View>

        <View>
          <Text
            style={{
              color: '#BB2427',
              fontSize: 12,
              fontWeight: '600',
              marginTop: 25,
              marginLeft: 21,
            }}>
            Catatan
          </Text>
          <TextInput
            style={{
              backgroundColor: '#F6F8FF',
              borderRadius: 7,
              marginHorizontal: 20,
              marginTop: 11,
              height: 92,
              textAlignVertical: 'top',
            }}
            value={catatan}
            onChangeText={text => setCatatan(text)}
            placeholder="Cth: ingin ganti sol baru"
          />
        </View>
      </ScrollView>

      <TouchableOpacity
        style={{
          //width: '100%',
          marginTop: 35,
          backgroundColor: '#BB2427',
          borderRadius: 8,
          paddingVertical: 15,
          justifyContent: 'center',
          alignItems: 'center',
          marginBottom: 34,
          marginHorizontal: 20,
        }}
        onPress={() => cartData()}>
        <Text
          style={{
            color: '#fff',
            fontSize: 16,
            fontWeight: 'bold',
          }}>
          {route.params ? 'Ubah' : 'Masukkan Keranjang'}
        </Text>
      </TouchableOpacity>

      {route.params && (
        <TouchableOpacity
          style={{
            //width: '100%',
            //marginTop: 35,
            backgroundColor: '#BB2427',
            borderRadius: 8,
            paddingVertical: 15,
            justifyContent: 'center',
            alignItems: 'center',
            marginBottom: 34,
            marginHorizontal: 20,
          }}
          onPress={deleteCart}>
          <Text
            style={{
              color: '#fff',
              fontSize: 16,
              fontWeight: 'bold',
            }}>
            Hapus
          </Text>
        </TouchableOpacity>
      )}
    </View>
  );
};

export default FormulirScreen;
