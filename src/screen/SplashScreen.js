import React, {useEffect} from 'react';
import {View, Text, Image} from 'react-native';

const SplashScreen = ({navigation, route}) => {
  useEffect(() => {
    setTimeout(() => {
      navigation.navigate('AuthNavigation');
    }, 1000);
  });
  return (
    <View
      style={{
        flex: 1,
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center',
      }}>
      <Image
        source={require('../assets/images/splashscreen.png')}
        style={{width: 150, height: 150, resizeMode: 'contain'}}
      />
    </View>
  );
};

export default SplashScreen;
