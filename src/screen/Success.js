import React from 'react';
import {View, Image, TouchableOpacity, ScrollView, Text} from 'react-native';

const SuccessScreen = ({navigation, route}) => {
  return (
    <View style={{flex: 1, backgroundColor: 'white'}}>
      <TouchableOpacity
        onPress={() =>
          navigation.navigate('HomeNavigation', {screen: 'Summary'})
        }>
        <View>
          <Image
            source={require('../assets/icons/close.png')}
            style={{
              tintColor: '#2E3A59',
              width: 14,
              height: 14,
              marginTop: 27,
              marginLeft: 21,
            }}
          />
        </View>
      </TouchableOpacity>

      <ScrollView>
        <View style={{alignItems: 'center', marginTop: 176}}>
          <Text
            style={{
              color: '#11A8AE',
              fontSize: 18,
              fontWeight: '700',
              lineHeight: 17,
              letterSpacing: 0.257,
              textAlign: 'center',
            }}>
            Reservasi Berhasil
          </Text>
          <Image
            source={require('../assets/icons/Ceklis.png')}
            style={{width: 160, height: 160, marginTop: 40}}
          />
          <Text
            style={{
              color: '#000',
              fontSize: 18,
              fontWeight: '400',
              lineHeight: 17,
              letterSpacing: 0.257,
              marginHorizontal: 34,
              marginTop: 55,
            }}>
            Kami Telah Mengirimkan Kode Reservasi ke Menu Transaksi
          </Text>
        </View>
      </ScrollView>

      <TouchableOpacity
        style={{
          //width: '100%',
          marginTop: 35,
          backgroundColor: '#BB2427',
          borderRadius: 8,
          paddingVertical: 15,
          justifyContent: 'center',
          alignItems: 'center',
          marginBottom: 34,
          marginHorizontal: 20,
        }}
        onPress={() => navigation.navigate('Transaction')}>
        <Text
          style={{
            color: '#fff',
            fontSize: 16,
            fontWeight: 'bold',
          }}>
          Lihat Kode Reservasi
        </Text>
      </TouchableOpacity>
    </View>
  );
};

export default SuccessScreen;
